package com.example.accessingdatamysql.container;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class ProductDto {

    private Long id;

    private String code;

    private String name;

    private String description;

    private int price;

    private int quantity;

    private String inventoryStatus;

    private String category;

    private String image;

    private int rating;
}
