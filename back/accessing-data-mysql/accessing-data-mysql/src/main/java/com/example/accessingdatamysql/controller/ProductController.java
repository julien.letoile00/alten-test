package com.example.accessingdatamysql.controller;

import com.example.accessingdatamysql.container.ProductDto;
import com.example.accessingdatamysql.model.Product;
import com.example.accessingdatamysql.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/app/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping(path = "/")
    public @ResponseBody ResponseEntity<ProductDto> createNewProduct(@RequestBody ProductDto productDto) {
        ProductDto createdProduct = productService.createNewProduct(productDto);
        HttpStatus status = HttpStatus.CREATED;

        if(createdProduct == null) {
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(createdProduct, status);
    }

    @GetMapping(path = "/")
    public @ResponseBody ResponseEntity<List<ProductDto>> retrieveAllProducts() {
        List<ProductDto> productDtos = productService.retrieveAllProducts();
        HttpStatus status = HttpStatus.FOUND;

        if(productDtos == null) {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(productDtos, status);
    }

    @GetMapping(path = "/{productId}")
    public @ResponseBody ResponseEntity<ProductDto> retrieveProductDetails(@PathVariable Long productId) {
        ProductDto productDto = productService.retrieveProductDetails(productId);
        HttpStatus status = HttpStatus.FOUND;

        if(productDto == null) {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(productDto, status);
    }

    @PatchMapping(path = "/{productId}")
    public @ResponseBody ResponseEntity<ProductDto> updateProductDetails(@PathVariable Long productId, @RequestBody ProductDto productDto) {
        ProductDto updatedProductDto = productService.updateProductDetails(productId, productDto);
        HttpStatus status = HttpStatus.ACCEPTED;

        if(updatedProductDto == null) {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(updatedProductDto, status);
    }

    @DeleteMapping(path = "/{productId}")
    public @ResponseBody ResponseEntity<Void> removeProduct(@PathVariable Long productId) {
        productService.removeProduct(productId);
        return ResponseEntity.ok().build();
    }
}
