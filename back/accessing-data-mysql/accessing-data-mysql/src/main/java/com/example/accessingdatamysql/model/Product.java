package com.example.accessingdatamysql.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    private String code;

    private String name;

    private String description;

    private int price;

    private int quantity;

    private String inventoryStatus;

    private String category;

    @Nullable
    private String image;

    private int rating;

}
