package com.example.accessingdatamysql.service;

import com.example.accessingdatamysql.model.Product;
import com.example.accessingdatamysql.repository.ProductRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class IO {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> readJsonProductFile() {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        List<Product> products = new ArrayList<>();

        try (FileReader reader = new FileReader("assets/products/products.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONObject data = (JSONObject) obj;

            JSONArray productList = (JSONArray) data.get("data");

            //Iterate over employee array
            productList.forEach( product -> {
                products.add(parseProductObject( (JSONObject) product ));
            } );

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return products;
    }

    private Product parseProductObject(JSONObject productObject)
    {
        Product product = new Product();

        product.setId((Long) productObject.get("id"));
        product.setCode((String) productObject.get("code"));
        product.setName((String) productObject.get("name"));
        product.setDescription((String) productObject.get("description"));
        product.setImage((String) productObject.get("image"));
        product.setPrice(((Long) productObject.get("price")).intValue());
        product.setCategory((String) productObject.get("category"));
        product.setQuantity(((Long) productObject.get("quantity")).intValue());
        product.setInventoryStatus((String) productObject.get("inventoryStatus"));
        product.setRating(((Long) productObject.get("rating")).intValue());

        return product;
    }
}
