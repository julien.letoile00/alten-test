package com.example.accessingdatamysql.service;

import com.example.accessingdatamysql.container.ProductDto;
import com.example.accessingdatamysql.mapper.ProductMapper;
import com.example.accessingdatamysql.model.Product;
import com.example.accessingdatamysql.repository.ProductRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private IO io;

    @Autowired
    private ProductRepository productRepository;

    @PostConstruct
    private void loadData() {
        List<Product> products = io.readJsonProductFile();
        List<Product> importedProducts = productRepository.saveAll(products);
        System.out.printf("Imported %d product(s)%n", importedProducts.size());
    }

    public ProductDto createNewProduct(final ProductDto productDto) {
        if(productDto == null) {
            return null;
        }

        Product product = ProductMapper.INSTANCE.productDtoToProduct(productDto);
        product.setId(null);
        Product createdProduct = productRepository.save(product);
        return ProductMapper.INSTANCE.productToProductDto(createdProduct);
    }

    public List<ProductDto> retrieveAllProducts() {
        List<Product> products = productRepository.findAll();
        return ProductMapper.INSTANCE.productListToProductDtoList(products);
    }

    public ProductDto retrieveProductDetails(final Long productId) {
        if(productId == null) {
            return null;
        }

        Optional<Product> product = productRepository.findById(productId);
        return product.map(ProductMapper.INSTANCE::productToProductDto).orElse(null);
    }

    public ProductDto updateProductDetails(final Long productId, ProductDto productDto) {
        if(productId == null) {
            return null;
        }

        Optional<Product> product = productRepository.findById(productId);
        if(product.isPresent()) {
            productDto.setId(productId);
            Product newProduct = productRepository.save(ProductMapper.INSTANCE.productDtoToProduct(productDto));
            return ProductMapper.INSTANCE.productToProductDto(newProduct);
        }

        return null;
    }

    public void removeProduct(final Long productId) {
        if(productId != null) {
            productRepository.deleteById(productId);
        }
    }
}
